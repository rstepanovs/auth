package lv.romans.meters.auth

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User not found")
object UserNotFoundException: Exception()

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Given passwords dont match")
object PasswordsDoNotMatchException: Exception()

@ResponseStatus(HttpStatus.FORBIDDEN)
object InvalidSignature : Exception()