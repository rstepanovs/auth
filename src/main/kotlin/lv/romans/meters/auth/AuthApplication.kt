package lv.romans.meters.auth

import com.auth0.jwt.algorithms.Algorithm
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.gson.Gson
import lv.romans.meters.auth.data.UserService
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import javax.annotation.PostConstruct


@EnableDiscoveryClient
@SpringBootApplication
class AuthApplication {

    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

    @Bean
    fun signingAlgorithm(): Algorithm {
        return Algorithm.HMAC256("a767d4530b5ed920e88f56193bb36729be39297858406f634426ef9223d0ae90")
    }

    @Bean
    fun jsonMessageConverter(): MessageConverter = Jackson2JsonMessageConverter(ObjectMapper().registerModule(KotlinModule()))

    @Autowired
    lateinit var userService: UserService

    @PostConstruct
    fun addAdmin() {
        userService.createAdminIfNeeded()
    }
}

fun main(args: Array<String>) {
    runApplication<AuthApplication>(*args)
}