package lv.romans.meters.auth.security

import lv.romans.meters.commons.Constants
import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthenticationFilter(authManager: AuthenticationManager, private val authToken: AuthToken) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val bearer: String? = request.getHeader(Constants.HEADER_AUTHORIZATION)
        if (bearer != null) {
            val authentication: UsernamePasswordAuthenticationToken? = tokenAuthentication(request)
            SecurityContextHolder.getContext().authentication = authentication
            chain.doFilter(request, response)
            return
        }
        val userEmail = request.getHeader(Constants.HEADER_USER_ID)
        val userRole = request.getHeader(Constants.HEADER_USER_ROLES)

        if (userEmail != null && userRole != null) {
            SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(userEmail, null, listOf(SimpleGrantedAuthority(userRole)))
            chain.doFilter(request, response)
            return
        }
        chain.doFilter(request, response)
    }

    private fun tokenAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token: String = request.getHeader(AUTHORIZATION)
        val userEmail = authToken.verify(token).subject
        return UsernamePasswordAuthenticationToken(userEmail, null, ArrayList())
    }
}