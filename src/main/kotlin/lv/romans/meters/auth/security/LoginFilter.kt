package lv.romans.meters.auth.security

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import lv.romans.meters.auth.data.User
import lv.romans.meters.commons.api.Login
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class LoginFilter(private val token: AuthToken, manager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    init {
        authenticationManager = manager
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        val creds = request.inputStream.bufferedReader().readText()
        val userCredentials = jacksonObjectMapper().readValue(creds, Login::class.java)
        return authenticationManager.authenticate(UsernamePasswordAuthenticationToken(userCredentials.email, userCredentials.password))
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        val user = authResult.principal as User
        token.attachToken(user, response)
        SecurityContextHolder.getContext().authentication = authResult;
    }
}