package lv.romans.meters.auth.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import lv.romans.meters.auth.data.User
import lv.romans.meters.auth.data.UserService
import lv.romans.meters.commons.Constants
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*
import javax.servlet.http.HttpServletResponse

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfiguration(private val userService: UserService, private val encoder: PasswordEncoder, private val authToken: AuthToken) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable()
                .addFilter(AuthenticationFilter(authenticationManager(), authToken))
                .addFilter(LoginFilter(authToken, authenticationManager()))
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userService).passwordEncoder(encoder)
    }

    override fun userDetailsService() = userService

    @Bean
    override fun authenticationManager() = super.authenticationManager()
}

@Component
data class AuthToken(private val algorithm: Algorithm) {

    fun attachToken(user: User, response: HttpServletResponse) {
        response.addHeader(Constants.HEADER_AUTHORIZATION, "${Constants.AUTH_PREFIX}${createToken(user)}");
    }

    fun createToken(user: User) =
            JWT.create().withSubject(user.username)
                    .withClaim(Constants.CLAIM_ROLE, user.role.name)
                    .withClaim(Constants.PASSWORD_CHANGE_REQUIRED, user.passwordChangeRequired)
                    .withExpiresAt(Date.from(Instant.now().plusSeconds(1800)))
                    .sign(algorithm)

    fun verify(bearer: String) = JWT.require(algorithm).build().verify(bearer.removePrefix(Constants.AUTH_PREFIX))
}