package lv.romans.meters.auth.controllers

import lv.romans.meters.auth.data.UserRepository
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*

@Profile("test")
@RestController
@RequestMapping("test")
class TestController(private val repo: UserRepository, private val passwordEncoder: PasswordEncoder) {

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("revertPasswordChange")
    fun revertPasswordChange(@AuthenticationPrincipal userEmail: String, @RequestBody password: String) {
        repo.save(repo.findByEmail(userEmail)!!.copy(passwordChangeRequired = true, pass = passwordEncoder.encode(password)))
    }

}
