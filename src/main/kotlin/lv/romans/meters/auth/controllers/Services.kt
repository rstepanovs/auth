package lv.romans.meters.auth.controllers

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.SignatureVerificationException
import lv.romans.meters.auth.InvalidSignature
import lv.romans.meters.auth.data.UserService
import lv.romans.meters.auth.security.AuthToken
import lv.romans.meters.commons.Constants
import lv.romans.meters.commons.UserRole
import lv.romans.meters.commons.api.ChangePassword
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

@RestController
class Services(private val algorithm: Algorithm, private val userService: UserService, private val tokenOperations: AuthToken) {

    @ResponseStatus(HttpStatus.ACCEPTED)
    @GetMapping("/validate")
    fun validateToken(@RequestHeader("Authorization") auth: String) {
        val token = auth.removePrefix(Constants.AUTH_PREFIX)
        try {
            algorithm.verify(JWT.decode(token))
        } catch (e: SignatureVerificationException) {
            throw InvalidSignature
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping("/changePassword")
    fun changePassword(@RequestBody newPassword: ChangePassword, @RequestHeader("Authorization") auth: String, response: HttpServletResponse) {
        val token = auth.removePrefix(Constants.AUTH_PREFIX)
        val decodedJWT = JWT.decode(token)
        try {
            algorithm.verify(decodedJWT)
        } catch (e: SignatureVerificationException) {
            throw InvalidSignature
        }
        val updatedUser = userService.changeUserPassword(decodedJWT.subject, newPassword)
        tokenOperations.attachToken(updatedUser, response)
    }

    @PostMapping("/manager/add")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun createManager(@RequestBody email: String) {
        userService.createUser(email, UserRole.ROLE_MANAGER)
    }
    @PostMapping("/owner/add")
    @PreAuthorize("hasAuthority('ROLE_MANAGER')")
    fun createUser(@RequestBody email: String) {
        userService.createUser(email, UserRole.ROLE_USER)
    }

    @DeleteMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    fun deleteUser(@RequestParam("email") email: String) {
        userService.deleteUser(email)
    }
}