package lv.romans.meters.auth.data

import lv.romans.meters.auth.PasswordsDoNotMatchException
import lv.romans.meters.auth.UserNotFoundException
import lv.romans.meters.auth.security.AuthToken
import lv.romans.meters.commons.UserRole
import lv.romans.meters.commons.api.ChangePassword
import lv.romans.meters.commons.api.Login
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.repository.CrudRepository
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import javax.persistence.*

@Service
@Transactional
class UserService(private val repository: UserRepository, private val encoder: PasswordEncoder,
                  private val token: AuthToken, @Value("\${domain}") private val domain: String,
                  @Value("\${adminPassword}") private val defaultAdminPassword: String,
private val mqTemplate: AmqpTemplate) : UserDetailsService {

    override fun loadUserByUsername(email: String): UserDetails {
        val user = repository.findByEmail(email)
        return user!!
    }

    fun createAdminIfNeeded() {
        val adminEmail = "admin@$domain"
        if (repository.findByEmail(adminEmail) == null) {
            repository.save(User(0, adminEmail, encoder.encode(defaultAdminPassword), UserRole.ROLE_ADMIN))
        }
    }

    fun changeUserPassword(userEmail: String, newPassword: ChangePassword): User {
        if (newPassword.newPassword != newPassword.newPasswordRepeated) throw PasswordsDoNotMatchException
        val user = repository.findByEmail(userEmail) ?: throw UserNotFoundException
        return repository.save(user.copy(pass = encoder.encode(newPassword.newPassword), passwordChangeRequired = false))
    }

    fun createUser(email: String, role: UserRole) {
        val tempPassword = UUID.randomUUID().toString().replace("-", "").substring(0, 10)

        repository.save(User(0, email, encoder.encode(tempPassword), role))
        mqTemplate.convertAndSend("user_creation_email", Login(email, tempPassword))
    }

    fun deleteUser(email: String) {
        repository.deleteByEmail(email)
    }
}

@Repository
interface UserRepository : CrudRepository<User, Long> {
    fun findByEmail(email: String): User?
    fun deleteByEmail(email: String)
}


@Entity
data class User(@Id @GeneratedValue val id: Long, @Column(unique = true) val email: String, val pass: String,
                @Enumerated(EnumType.STRING) val role: UserRole, val passwordChangeRequired: Boolean = true) : UserDetails {
    override fun getPassword() = pass
    override fun getUsername() = email
    override fun getAuthorities() = listOf(SimpleGrantedAuthority(role.name))

    override fun isAccountNonExpired() = true
    override fun isAccountNonLocked() = true
    override fun isCredentialsNonExpired() = true
    override fun isEnabled() = true
}