package lv.romans.meters.auth.async

import lv.romans.meters.auth.data.UserService
import lv.romans.meters.commons.messaging.NewUser
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.context.annotation.Configuration

@Configuration
class RabbitConfiguration(private val service: UserService) {

    @RabbitListener(queues = ["user_creation"])
    fun createUser(user: NewUser) {
        service.createUser(user.email, user.role)
    }

    @RabbitListener(queues = ["user_deletion"])
    fun deleteUser(email: String) {
        service.deleteUser(email)
    }

}